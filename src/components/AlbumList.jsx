import { useState, useEffect } from 'react';
import Album from './Album';
import styles from './AlbumList.module.css';

function AlbumList(props) {
  const [albums, setAlbums] = useState(['A', 'B', 'C', 'D', 'E']);

  useEffect(() => {
    const newAlbums = [...props.albums];

    const interval = setInterval(() => {
      setAlbums(prevState => {
        const prevAlbums = [...prevState];

        let firstAlbum = prevAlbums.shift();
        if (newAlbums.length > 0) {
          firstAlbum = newAlbums[0];
        }

        return [...prevAlbums, firstAlbum];
      });

      newAlbums.shift();
    }, 1000);

    return () => { clearInterval(interval); };
  }, [props.albums]);

  return (
    <div className={styles.albumList}>
      {albums.map(album => (
        <Album key={album} name={album} />
      ))}
    </div>
  );
}

export default AlbumList;
