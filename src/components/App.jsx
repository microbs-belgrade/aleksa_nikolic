import axios from 'axios';
import { useState, useEffect } from 'react';
import AlbumList from './AlbumList';
import styles from './App.module.css';

function App() {
  const [albums, setAlbums] = useState([]);
  const [search, setSearch] = useState('');

  useEffect(() => {
    if (search !== '') {
      axios('https://itunes.apple.com/search', {
        params: {
          term: search,
          country: 'US',
          media: 'music',
        },
      }).then(response => {
        const songs = response.data.results;
        const albumNames = [...songs.map(song => song.collectionName + '')];
        const albumNamesUnique = [...new Set(albumNames)];

        albumNamesUnique.sort();

        setAlbums(albumNamesUnique.slice(0, 5));
      }).catch(error => {
        console.log(error.message);
      });
    }
  }, [search]);

  return (
    <div className={styles.container}>
      <input
        className={styles.bandInput}
        type='search'
        placeholder='Search Band'
        value={search}
        onChange={(e) => { setSearch(e.target.value); }}
      />
      <AlbumList albums={albums} />
    </div>
  );
}

export default App;
