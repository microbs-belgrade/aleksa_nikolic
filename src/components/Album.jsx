import styles from './Album.module.css';

function Album({ name }) {
  return (
    <div className={styles.album}>
      {name}
    </div>
  );
}

export default Album;
